# Angular Training



Este projeto tem como intuito ajudar os iniciantes a aprenderem alguns paradigmas do framework Angular

  - Uso de Controllers
  - Uso de rotas
  - Uso de servicos

# Getting Started!!
É necessário ter possuir os seguintes softwares instalados
- NodeJs
- NPM
- Bower

Abra seu terminal na pasta root do projeto execute os seguintes comandos
```sh
$ npm install
$ bower install
```

Para rodar o projeto execute o seguinte comando
```sh
$ node server.js
```

Após isso estamos prontos para o desenvolvimento, você pode usar o codigo dos serviços e controlles já existentes para ter uma base.

![alt text](https://media.giphy.com/media/3o85xAojNshmzlySyc/giphy.gif)


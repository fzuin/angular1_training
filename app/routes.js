module.exports = function (app) {
	var instructions = 
		[
		{
			"id" : 1,
			"step": "Step 1",
			"text": "Consumir uma informação do back-end, uma boa dica é ler sobre $http, utilize o RegisterCtrl e o RegisterService,  exiba a resposta em um alert(). ",
			"endpoint": "Metodo : GET | Route: /test",
			"response": '{message : "Hello World"}',
			"exemplo": '/img/exercicio_1.png'
		},
		{
			"id" : 2,
			"step": "Step 2",
			"text": "Agora vamos consumir usando o mesmo metodo mas um endpoint diferente, e popular nosso formulário usando o ng-model, ou seja para que ele se autopopule após o request",
			"endpoint": "Metodo : GET | Route: /cliente/123",
			"response": '{"id": 123,"nome": "Fabio Zuin","cidade": "Jundiai","idade": 21,"estado_civil": "Solteiro"}',
			"exemplo": '/img/exercicio_2.png'
		},
		{
			"id" : 3,
			"step": "Step 3",
			"text": "Agora iremos mandar o cliente para o servidor, fazendo um post podemos mandar as informações para o back-end, então receberemos uma mensagem de sucesso ou de falha, a mesma devera ser mostrada em um alert()",
			"endpoint": "Metodo : POST | Route: /cliente",
			"response": '{"message" : "Info do usuario Fabio Zuin salvo com sucesso ID 123"}',
			"exemplo": '/img/exercicio_3.png'
		},
		{
			"id" : 4,
			"step": "Step 4",
			"text": " Após o cadastro com sucesso, tente cadastrar o cliente com ID 666, receberemos um status de erro no HTTP e uma mensagem, exiba a mesma para o usuário",
			"endpoint": "Metodo : POST | Route: /cliente",
			"response": '{"message" : "Este usuário não é permitido"}',
			"exemplo": '/img/exercicio_4.png'
		},

		{
			"id" : 5,
			"step": "Step 5",
			"text": "Agora criaremos um novo controller, service, rota e html, tudo isso para criarmos uma nova pagina, sinta-se livre para adcionar uma table nessa pagina.",
			"endpoint": "Route: /lista",
			"response": 'Não há resposta para o momento',
			"exemplo": '/img/exercicio_5.png'
		},
		{
			"id" : 6,
			"step": "Step 6",
			"text": "Vamos popular a tabela com informações dos clientes que receberemos do back-end",
			"endpoint": "Metodo: GET | Route: /clientes/lista",
			"response": '[{"id": 123,"nome": "Fabio Zuin","cidade": "Jundiai","idade": 21,"estado_civil": "Solteiro"}, {"id": 123,"nome": "Fabio Zuin","cidade": "Jundiai","idade": 21,"estado_civil": "Solteiro"}]',
			"exemplo": '/img/exercicio_5.png'
		},
		{
			"id" : 7,
			"step": "Step 7",
			"text": "Adcione um  filtro a tabela, filtre pelo ID",
			"endpoint": "",
			"response": '',
			"exemplo": ''
		}

	]

	// server routes ===========================================================
	// handle things like api calls
	// authentication routes

	//Aqui temos um rest que retorna um json de teste

	app.get("/instructions", function (req, res) {
		res.send(instructions);
	})



	app.get("/test", function (req, res) {
		res.send({ "message": "Hello World" });
	});

	app.get("/cliente/:id", function (req, res) {
		let client = {
			"id": req.params.id,
			"nome": "Fabio Zuin",
			"cidade": "Jundiai",
			"idade": 21,
			"estado_civil": "Solteiro"
		}
		res.send(client);
	});

	app.post("/cliente", function (req, res) {
		if(req.body.id === "666"){
			res.status(500).send({"message" : "Este usuário não é permitido"});
		}else {
			res.status(200).send({ "message": "Info do usuario " + req.body.nome + " salvo com sucesso ID " + req.body.id });
		} 
	});

	app.get("/cliente/lista", function (req, res) {
		let clients = [
			{
				"id": "123",
				"nome": "Fabio Zuin",
				"cidade": "Jundiai",
				"idade": 21,
				"estado_civil": "Solteiro"
			},
			{
				"id": "531",
				"nome": "Jack Zuin",
				"cidade": "Jundiai",
				"idade": 21,
				"estado_civil": "Solteiro"
			},
			{
				"id": "543",
				"nome": "hern Zuin",
				"cidade": "Jundiai",
				"idade": 21,
				"estado_civil": "Solteiro"
			},
			{
				"id": "21312",
				"nome": "john Zuin",
				"cidade": "Jundiai",
				"idade": 21,
				"estado_civil": "Solteiro"
			}
		]
	});


	// frontend routes =========================================================
	// route to handle all angular requests
	app.get('*', function (req, res) {
		res.sendfile('./public/index.html');
	});

};
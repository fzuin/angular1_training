angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		// home page
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MainController',
			controllerAs : "vm"
		})

		.when('/register', {
			templateUrl: 'views/reg.html',
			controller: 'RegisterController',
			controllerAs : "vm"
		})


	$locationProvider.html5Mode(true);

}]);
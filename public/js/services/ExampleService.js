angular.module('ExampleModule', []).service('ExampleService', ['$http', '$q', function ($http, $q) {


    var getTest = function () {
        return $http.get('/instructions');
    }


    return {
        get: getTest
    }

}]);
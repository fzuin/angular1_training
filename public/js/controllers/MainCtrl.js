angular.module('MainCtrl', []).controller('MainController', ['$scope', 'ExampleService', '$q', '$cookies', '$cookieStore', function ($scope, ExampleService, $q, $cookies, $cookieStore) {

	var vm = this;
	vm.loader = 0;

	function loadInstructions() {
		var promesa = ExampleService.get();
		promesa.then(function (value) {
			vm.instructions = value.data;
		}, function (reason) {
			alert(reason);
		});
	}



	function checkTheLastStep() {

		console.log($cookies.getAll());
		exerNumber = $cookies.getAll();
		vm.exer = Object.keys(exerNumber).length;

	}

	vm.clearCookies = function () {
		var cookies = $cookies.getAll();
		angular.forEach(cookies, function (v, k) {
			$cookies.remove(k);
		});
		vm.loader = 0;
		checkTheLastStep();
	}

	vm.hello = function (step, id) {
		console.log(step);
		$cookies.putObject(step, "completed");
		vm.selectedIndex = id;
		checkTheLastStep();
		vm.loader = vm.loader + 15;
	}

	function init() {
		vm.exer = null;
		loadInstructions();
		checkTheLastStep();
	}

	init();
	return vm;
}]);	